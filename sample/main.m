//
//  main.m
//  sample
//
//  Created by Mcuser on 6/7/17.
//  Copyright © 2017 Vasilkoff LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
