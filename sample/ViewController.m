//
//  ViewController.m
//  sample
//
//  Created by Mcuser on 6/7/17.
//  Copyright © 2017 Vasilkoff LTD. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UILabel *someLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Calling a function
    [self MyFunction];
    float p2 = 1.5;
    
    
    
    // Call a function and catch result
    float a = [self Add:4.5 toSecond:p2];
    NSLog(@"%f", a);
    
    //Updated project
}

//Make a new function
//
- (void) MyFunction
{
    int A = 1;
    int B = 5;
    
    int plus = A + B;
    int minus = A - B;
    int multiplies = A * B;
    float devide = A / B;
    float modulus = A % B;
    
    int inc = B ++;
    int dec = B --;
    
    //Going to DEBUG
    
    float floatValue = 4.5;
    
    NSString *myString = @"Basic string";
}

//Make new function with return data type float
-(float) Add:(float)p1 toSecond:(float)p2
{
    float result;
    result = p1 + p2;
    return result;
}

- (IBAction)changeMessage:(id)sender {
    
    NSString *s = self.someLabel.text;
    
    if ([s isEqualToString:@"hello"])
    {
        self.someLabel.text = [NSString stringWithFormat:@"Result %f", 3.4f];
    } else {
        self.someLabel.text = @"hello";
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
